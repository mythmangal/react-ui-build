import React, { PropTypes } from 'react';
import cssModules from 'react-css-modules';
import styles from './index.module.scss';

const Header = ({
  title,
}) => (
  <div className={styles.header}>
    <h1>{title}</h1>
  </div>
);

Header.propTypes = {
  title: PropTypes.string.isRequired,
};

export default cssModules(Header, styles);
