import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import React from 'react';
import { spy } from 'sinon';
import Header from '../index';

describe('<Header />', () => {
  it('should render with default props', () => {
    const wrapper = shallow(
      <Header />
    );
    expect(shallowToJson(wrapper)).toMatchSnapshot();
  });
});
