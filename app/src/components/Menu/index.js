import React from 'react';
import cssModules from 'react-css-modules';
import { Link } from 'react-router';

import styles from './index.module.scss';

const Menu = () => (
  <div className={styles.menu}>
    <ul>
      <li><Link to="/">Home</Link></li>
      <li><Link to="/about">About</Link></li>
    </ul>
  </div>
);

export default cssModules(Menu, styles);
