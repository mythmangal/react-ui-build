import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import React from 'react';
import { spy } from 'sinon';
import Menu from '../index';

describe('<Menu />', () => {
  it('should render with default props', () => {
    const wrapper = shallow(
      <Menu />
    );
    expect(shallowToJson(wrapper)).toMatchSnapshot();
  });
});
