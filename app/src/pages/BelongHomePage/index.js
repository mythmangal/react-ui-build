import React from 'react';
import cssModules from 'react-css-modules';
import { BelongHomeContainer } from 'containers';  // eslint-disable-line
import styles from './index.module.scss';

const BelongHomePage = () => (
  <div className={styles.container}>
    <BelongHomeContainer />
  </div>
);

export default cssModules(BelongHomePage, styles);
