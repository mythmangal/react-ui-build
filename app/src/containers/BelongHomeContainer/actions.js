import * as types from './constants';

// belongHomedefaultAction :: None -> {Action}
export const belongHomeDefaultAction = () => ({ // eslint-disable-line
  type: types.BELONGHOME_DEFAULT_ACTION,
});
