import expect from 'expect';
import * as actions from '../actions';
import * as types from '../constants';

describe('BelongHome actions', () => {
  it('has a type of BELONGHOME_DEFAULT_ACTION', () => {
    const expected = {
      type: types.BELONGHOME_DEFAULT_ACTION,
    };
    expect(actions.belongHomeDefaultAction()).toEqual(expected);
  });
});
