import { shallow } from 'enzyme';
import { shallowToJson } from 'enzyme-to-json';
import React from 'react';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import { spy } from 'sinon';
import { initialState as belongHome } from '../reducer';
import BelongHomeContainer from '../index';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('<BelongHome />', () => {
  it('should render with default props', () => {
    const store = mockStore({ belongHome });
    const wrapper = shallow(
      <BelongHomeContainer store={store} />
    );
    expect(shallowToJson(wrapper)).toMatchSnapshot();
  });
});
