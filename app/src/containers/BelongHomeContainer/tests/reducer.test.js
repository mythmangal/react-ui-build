import expect from 'expect';
import * as types from '../constants';
import belongHomeReducer, { initialState } from '../reducer';

describe('belongHomeReducer', () => {
  it('returns the initial state', () => {
    expect(
      belongHomeReducer(undefined, {})
    ).toEqual(initialState);
  });
});
