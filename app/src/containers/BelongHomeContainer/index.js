import React, { Component, PropTypes } from 'react';
import cssModules from 'react-css-modules';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as BelongHomeActionCreators from './actions';
import styles from './index.module.scss';

class BelongHomeContainer extends Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className={styles.belongHome}>
        <h1>My Belong Home page</h1>
        <h2>{this.props.header}</h2>
        <p>{this.props.content}</p>
      </div>
    );
  }
}

BelongHomeContainer.propTypes = {
  header: PropTypes.node.string,
  content: PropTypes.node.string,
};

const mapStateToProps = (state) => ({ // eslint-disable-line
  header: state.belongHome.headerText,
  content: state.belongHome.content,
});

// mapDispatchToProps :: Dispatch -> {Action}
const mapDispatchToProps = (dispatch) => ({ // eslint-disable-line
  actions: bindActionCreators(
    BelongHomeActionCreators,
    dispatch,
  ),
});

const Container = cssModules(BelongHomeContainer, styles);


export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Container);

