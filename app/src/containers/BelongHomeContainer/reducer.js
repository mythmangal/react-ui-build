import * as types from './constants';

export const initialState = {
  // Initial State goes here!
  headerText: 'My default Header',
  content: 'Main content',
};

const belongHomeReducer =
  (state = initialState, action) => {
    switch (action.type) {
      case types.BELONGHOME_DEFAULT_ACTION:
        return state;
      default:
        return state;
    }
  };

export default belongHomeReducer;
