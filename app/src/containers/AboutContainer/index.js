import React, { Component } from 'react';
import cssModules from 'react-css-modules';
import styles from './index.module.scss';

class AboutContainer extends Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <div className={styles.about}>
        <p>About Page</p>
      </div>
    );
  }
}


const Container = cssModules(AboutContainer, styles);


export default Container;
